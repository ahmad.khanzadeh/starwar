import React from 'react';
import './App.css';
import Starwar from './Starwar';




function App() {
  return (
    <div className="App">
      <header className="App-header">
          <Starwar />
      </header>
    </div>
  );
}

export default App;
