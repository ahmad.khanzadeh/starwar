import React from "react";
import FilmItemRow from "./FilmItemRow";

class Starwar extends React.Component{
    constructor(){
      super()
  
      this.state={
        loadedCharacters:false,
        name:null,
        height:null,
        homeworld:null,
        films:[],
      }
    }
  
    getNewCharachter(){
    //  this api provides  82 different numbers 
    const randomNumber=Math.round(Math.random()*82)
      const url=`https://swapi.dev/api/people/${randomNumber}/`;
      const theUrl=`https://github.com/akabab/starwars-api/tree/master/api/${randomNumber}`;
      fetch(url).then(respons => respons.json()).then(data => {
        console.log("Request is already sent. Please be patient...")
        this.setState({
          loadedCharacters:true,
          name:data.name,
          height:data.height,
          homeworld: data.homeworld,
          films:data.films,
          
        })
      })
  
    
    }
    render(){
      const movies=this.state.films.map((url,i)=>{
        return <FilmItemRow url={url} key={i}/>
      })
  
  
      return(
        <div>
          {
            this.state.loadedCharacters && 
            <div> 
                <h1>Name:{this.state.name}</h1>
                <h2>height(cm):{this.state.height}</h2>
                <p>Homeworld(url):<a href={this.state.homeworld}>{this.state.homeworld}</a></p>
                <ul>
                  {/* {
                    this.state.films.map((singleFilm, filmKey) =>{
                      return <li key={filmKey}><a href={singleFilm}>{singleFilm}</a></li>
                    })
                  } */}
                  {movies}
                </ul>
            </div>
          }
  
          <button 
          className='btn'
           onClick={() =>this.getNewCharachter()} 
           type='button' 
           >
             Randomize BTN
          </button>
        </div>
      )
    }
  }

  export default Starwar